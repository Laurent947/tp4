jQuery(function($){
	var total=0;
	var price=0;
    var number=1;
    var degree_celsius;
    var cpt=0;
	// 3) Afficher les ingrédients des pizzas au survol de leurs noms
	$("label").hover(
	function(){
		var element=$(this).find('span.no-display')
		element.css("display","inline-block");
		element.css("color","blue");
		element.css("font-size","15px");
	},
	function(){
		$( this).find( "span.no-display" ).css("display","none");
	});
	// 4) Afficher visuellement le nombre de parts et le nombre de pizzas approprié en dessous du champs « Nombre de parts »
	$("input[type='text']").change(
		function affichePizza(){
			$("div.nb-parts:last").children().last().removeClass();
			$("span.pizza-6").remove();
			number = +$(this).val();
			var nbPart=number%6;
			var nbPizza=Math.floor(number/6)
			if(number<=6){
				$("div.nb-parts:last").children().last().addClass("pizza-"+number+" pizza-pict");
			}
            else if(number>6){
            	while(nbPizza>0){
            		nbPizza--;
				    $(this).next().before("<span class='pizza-6 pizza-pict'></span>");
			    }
			    switch(nbPart){
			    	case 0:
				    $("div.nb-parts:last").children().last().addClass('pizza-0 pizza-pict');
				    break;

				    case 1:
				    $("div.nb-parts:last").children().last().addClass('pizza-1 pizza-pict');
				    break;

				    case 2:
				    $("div.nb-parts:last").children().last().addClass('pizza-2 pizza-pict');
				    break;

				    case 3:
				    $("div.nb-parts:last").children().last().addClass('pizza-3 pizza-pict');
				    break;

				    case 4:
				    $("div.nb-parts:last").children().last().addClass('pizza-4 pizza-pict');
				    break;

				    case 5:
				    $("div.nb-parts:last").children().last().addClass('pizza-5 pizza-pict');
				    break;

				    case 6:
				    $("div.nb-parts:last").children().last().addClass('pizza-6 pizza-pict');
				    break;
			    }
		    }
	    });

	// 5) Afficher le formulaire de saisie d'adresse au clic sur le bouton "Etape suivante" puis masquer ce même bouton
	$("button:last").click(function(){
		$("div.no-display").css("display","block");
		$(this).remove();

	});


    // 6) Ajouter une ligne de champ d'adresse lorsque l'on clique sur le bouton "Ajouter un autre ligne d'adresse"
	$("button.btn-default").click(function(){
		$(this).before('<br><input type="text"/>')
	});
	$("input[type='text']").change(
		function getName(){
		var name=$("input").eq(-3).val();

	    

    /* 7) Au clic sur le bouton de validation, supprimer tous les éléments de la page, 
    et afficher un message de remerciement (Merci PRENOM ! Votre commande sera livrée dans 15 minutes).*/
	$("button.done").click(function(){
		$("h1").after("<span> Merci "+ name + "! Votre commande sera livrée dans 15 minutes.</span><br/>");
	    $("div.row:not(input:eq(-3))").remove();
		
	});

	});


	//8 - Actualiser le total de la commande en fonction des éléments choisis grâce à l'attribut data-price. (Je n'ai pas réussi dans une seule fonction le calcul)
	$('input').change(function(){
     
	$('input[name=type]:checked').each(function(){
		total=0;
		price= +$(this).attr('data-price')
		total=total + (price*number)/6;
    });

    $('input[name=pate]:checked').each(function() {
		price= +$(this).attr('data-price')
		total=total + price;
    });

    $('input[type=checkbox]:checked').each(function() {
     	if($(this).prop('checked', true)){
     		price= +$(this).attr('data-price')
		    total=total + price;
     	}
     	else{
     		price=0;
     	}
    });

    $("p:last").text(total + "€");
    });  
    
});